#ifndef DATATYPES_H
#define DATATYPES_H


template <class T>
struct Node
{
    T value;
    Node *next;

    Node(T val) : value(val) {next = nullptr;}
    ~Node() {(next != nullptr) ? next->~Node() : delete this;}
};

template <class T>
class LinkedList
{

    LinkedList();
    LinkedList(T val);
    ~LinkedList();

    public:
    int length() { return size; }

    T append(T val);
    T prepend(T val);
    void print();

    T operator[](unsigned int index);
    T operator()(int index);

    private:
    int size;
    Node<T> *head;
    Node<T> *tail;
};

#endif // DATATYPES_H
