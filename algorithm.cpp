#include "algorithm.h"

floatt Newton(floatt x, floatt (*f)(floatt), floatt (*dfdx)(floatt), floatt eps)
{
    floatt y = 1e5;
    floatt y_prev = x;
    while (y > eps)
    {
        y = y_prev + f(x) / dfdx(x);
        y_prev = y;
    }
    return y;
}

floatt Integrate(floatt (*f)(floatt), floatt a, floatt b, int n)
{
    floatt dx = (b - a)/n;
    floatt result = f(a) + f(b);
    for (int i = 1; i < n-1; i++)
        result += 2*(i%2 + 1) * f(a + i*dx);
    return (dx/3)*result;
}


RungeKutta4::RungeKutta4(floatt x_0, floatt y_0, floatt dx)
    : x_prev(x_0), y_prev(y_0), dx(dx)
{
    parent = nullptr;
    y = y_0;
    x = x_0;
}

RungeKutta4::RungeKutta4(RungeKutta4 *RK, floatt x_0, floatt y_0, floatt dx)
    : x_prev(x_0), y_prev(y_0), dx(dx)
{
    parent = RK;
    y = y_0;
    x = x_0;
}

RungeKutta4::~RungeKutta4()
{
    (parent != nullptr) ? parent->~RungeKutta4() : delete this;

}

floatt RungeKutta4::step()
{
    y_prev = (parent != nullptr) ? parent->step() : y_prev;

    floatt k1 = dx * f(x_prev, y_prev);
    floatt k2 = dx * f(x_prev + .5*dx, y_prev + .5*k1);
    floatt k3 = dx * f(x_prev + .5*dx, y_prev + .5*k2);
    floatt k4 = dx * f(x_prev + dx, y_prev + k3);

    x = x_prev + dx;
    y = y_prev + (k1 + 2*k2 + 2*k3 + k4) / 6.;

    return y;
}
