#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "macros.h"
#include "general.h"

floatt Newton(floatt x, floatt (*f)(floatt), floatt (*dfdx)(floatt), floatt eps=1e-7);

floatt Integrate(floatt (*f)(floatt), floatt a, floatt b, int n=1e5);

class RungeKutta4
{
    floatt x;
    floatt y;
    floatt x_prev;
    floatt y_prev;
    floatt dx;

    RungeKutta4 *parent;

    RungeKutta4(floatt x_0, floatt y_0, floatt dx=1e-4);
    RungeKutta4(RungeKutta4 *RK, floatt x_0, floatt y_0, floatt dx=1e-4);

    virtual ~RungeKutta4();

    virtual floatt f(floatt x, floatt y) = 0;

    public:
    floatt step();

    floatt getx() { return x; }
    floatt gety() { return y; }
    floatt getPrevx() { return x_prev; }
    floatt getPrevy() { return y_prev; }
};

#endif // ALGORITHM_H
