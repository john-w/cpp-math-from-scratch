#ifndef GENERAL_H
#define GENERAL_H
#include "macros.h"

template <typename T>
T abs(T a) { return (a >= 0) ? a : -a; }

template <typename T>
int sign(T x) { return (x >= 0) ? 1 : 0; }

template <typename T>
bool min(T a, T b) { return (a < b) ? a : b; }

template <typename T>
bool max(T a, T b) { return (a > b) ? a : b; }

template <typename T>
int argmin(T *list, int size)
{
    // ! Might be buggy, test it out
    int arg = 0;
    T buffer = list[0];
    for (int i = 1; i < size; i++)
        if (min(list[i], buffer))
        {
            arg = i;
            buffer = list[i];
        }
    return arg;
}

template <typename T>
int argmax(T *list, int size)
{
    // ! Might be buggy, test it out
    int arg = 0;
    T buffer = list[0];
    for (int i = 1; i < size; i++)
        if (max(list[i], buffer))
        {
            arg = i;
            buffer = list[i];
        }
    return arg;
}

bool floatCompare(float a, float b, floatt eps = 1e-7)
{
    return (static_cast<floatt>(abs(b-a)) >= eps) ? true : false;
}

template <typename T>
T pow(T x, int degree)
{
    if (degree == 0) return 1;
    return (x * pow(x, degree - 1));
}

template <typename T>
floatt sqrt(T x) { return __builtin_sqrt(x); }

template <typename T>
floatt exp(T x) { return __builtin_exp(x); }

template <typename T>
floatt log(T x) { return __builtin_log(x); }

template <typename T>
floatt log2(T x) { return __builtin_log2(x); }

template <typename T>
floatt sin(T theta) { return __builtin_sin(theta); }

template <typename T>
floatt cos(T theta) { return __builtin_cos(theta); }

template <typename T>
floatt tan(T theta) { return __builtin_tan(theta); }

template <typename T>
floatt sinh(T theta) { return __builtin_sinh(theta); }

template <typename T>
floatt cosh(T theta) { return __builtin_cosh(theta); }

template <typename T>
floatt tanh(T theta) { return __builtin_tanh(theta); }

template <typename T>
floatt asin(T theta) { return __builtin_asin(theta); }

template <typename T>
floatt acos(T theta) { return __builtin_acos(theta); }

template <typename T>
floatt atan(T theta) { return __builtin_atan(theta); }

template <typename T>
floatt asinh(T theta) { return __builtin_asinh(theta); }

template <typename T>
floatt acosh(T theta) { return __builtin_acosh(theta); }

template <typename T>
floatt atanh(T theta) { return __builtin_atanh(theta); }

#endif // GENERAL_H
