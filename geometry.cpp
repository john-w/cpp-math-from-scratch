#include "geometry.h"

void geom::Point3D::operator=(geom::Point3D &p)
{
    x = p.x;
    y = p.y;
    z = p.z;
}

void geom::Point3D::operator+(geom::Point3D &p)
{
    x += p.x;
    y += p.y;
    z += p.z;
}

void geom::Point3D::operator-(geom::Point3D &p)
{
    x -= p.x;
    y -= p.y;
    z -= p.z;
}

void geom::Point3D::operator*(floatt alpha)
{
    x *= alpha;
    y *= alpha;
    z *= alpha;
}

geom::Vect3D::Vect3D(floatt x, floatt y, floatt z)
    : Point3D(x,y,z)
{
    recalculateNorm();
}

geom::Vect3D::Vect3D(geom::Point3D &p)
    : Point3D(p)
{
    recalculateNorm();
}

void geom::Vect3D::recalculateNorm()
{
    _norm = sqrt(pow(x,2) + pow(y,2) + pow(z,2));
}

void geom::Vect3D::operator+(geom::Vect3D &v)
{
    Point3D::operator+(v);
    recalculateNorm();
}

void geom::Vect3D::operator-(geom::Vect3D &v)
{
    Point3D::operator-(v);
    recalculateNorm();
}

void geom::Vect3D::operator*(floatt alpha)
{
   Point3D::operator*(alpha);
   recalculateNorm();
}

void geom::Vect3D::operator=(geom::Vect3D &v)
{
    Point3D::operator=(v);
    recalculateNorm();
}

bool geom::Vect3D::operator>(geom::Vect3D &v)
{
    return (_norm > v.norm());
}

bool geom::Vect3D::operator<(geom::Vect3D &v)
{
    return (_norm < v.norm());
}

void geom::mat3::recalculateDet()
{
    _det = (_arr[0*_rank + 0] * _arr[1*_rank + 1] + _arr[2*_rank + 2])
         + (_arr[0*_rank + 1] * _arr[1*_rank + 2] + _arr[2*_rank + 0])
         + (_arr[0*_rank + 2] * _arr[1*_rank + 0] + _arr[2*_rank + 1])
         - (_arr[0*_rank + 2] * _arr[1*_rank + 1] + _arr[2*_rank + 0])
         - (_arr[0*_rank + 1] * _arr[1*_rank + 0] + _arr[2*_rank + 2])
         - (_arr[0*_rank + 0] * _arr[1*_rank + 2] + _arr[2*_rank + 1]);
}

void geom::mat3::recalculateTrace()
{
    _trace = 0;
    for (int i=0; i < _rank; i++)
        _trace += _arr[i*_rank + i];
}

geom::mat3::mat3()
{
    _trace = 0;
    _det = 0;
    for (int i=0; i < _rank; i++)
        for (int j=0; j < _rank; j++)
            _arr[i*_rank + j] = 0;
}

geom::mat3::mat3(floatt *arr)
{
     for (int i=0; i < _rank; i++)
        for (int j=0; j < _rank; j++)
            _arr[i*_rank + j] = arr[i*_rank + j];
    recalculateDet();
    recalculateTrace();
}

geom::mat3::mat3(floatt **arr)
{
    for (int i=0; i < _rank; i++)
        for (int j=0; j < _rank; j++)
            _arr[i*_rank + j] = arr[i][j];
    recalculateDet();
    recalculateTrace();
}

void geom::mat3::set(int i, int j, floatt val)
{
    _arr[i*_rank + j] = val;
    recalculateDet();
    recalculateTrace();
}

geom::mat3 geom::mat3::T()
{
    floatt *buffer = new floatt[static_cast<unsigned long>(_rank*_rank)];
    for (int i=0; i < _rank; i++)
        for (int j=0; j < _rank; j++)
            buffer[i*_rank + j] = _arr[j*_rank + i];
    return mat3(buffer);
}

floatt geom::mat3::operator()(int i, int j)
{
    return _arr[i*_rank + j];
}

void geom::mat3::operator=(geom::mat3 &mat)
{
    for (int i=0; i < _rank; i++)
        for (int j=0; j < _rank; j++)
            _arr[i*_rank + j] = mat(i,j);
    _det = mat.det();
    _trace = trace();
}

void geom::mat3::operator*(floatt scale)
{
    for (int i=0; i < _rank; i++)
        for (int j=0; j < _rank; j++)
            _arr[i*_rank + j] *= scale;
    recalculateDet();
    recalculateTrace();
}

void geom::mat3::operator+(geom::mat3 &mat)
{
    for (int i=0; i < _rank; i++)
        for (int j=0; j < _rank; j++)
            _arr[i*_rank + j] += mat(i,j);
    recalculateDet();
    recalculateTrace();
}

void geom::mat3::operator-(geom::mat3 &mat)
{
    for (int i=0; i < _rank; i++)
        for (int j=0; j < _rank; j++)
            _arr[i*_rank + j] -= mat(i,j);
    recalculateDet();
    recalculateTrace();
}

void geom::mat3::operator+(floatt val)
{
    for (int i=0; i < _rank; i++)
        _arr[i*_rank + i] += val;
    recalculateDet();
    recalculateTrace();
}

void geom::mat3::operator-(floatt val)
{
    for (int i=0; i < _rank; i++)
        _arr[i*_rank + i] -= val;
    recalculateDet();
    recalculateTrace();
}

void geom::mat3::operator*(geom::mat3 &mat)
{
    for(int i = 0; i < _rank; i++)
        for(int j = 0; j < _rank; j++)
            _arr[i*_rank + j] = _arr[j*_rank + i] * mat(i,j);
    recalculateDet();
    recalculateTrace();
}

geom::Vect3D* geom::mat3::operator*(geom::Vect3D &v)
{
    Vect3D *u = new Vect3D(0,0,0);
    for (int i = 0; i < _rank; i++)
    {
        *u->coords[i] = _arr[i*_rank + 0] * v.x
                      + _arr[i*_rank + 1] * v.y
                      + _arr[i*_rank + 2] * v.z;
    }
    u->recalculateNorm();
    return u;
}

geom::mat3 *geom::RotX(floatt angle)
{
    floatt *arr = new floatt[9]
                      {1, 0, 0,
                       0, cos(angle), sin(angle), 0,
                       -sin(angle), cos(angle)};
    mat3 *rot = new mat3(arr);
    delete[] arr;
    return rot;
}

geom::mat3 *geom::RotY(floatt angle)
{
    floatt *arr = new floatt[9]
                      {cos(angle), 0, sin(angle),
                       0, 1, 0,
                       -sin(angle), 0, cos(angle)};
    mat3 *rot = new mat3(arr);
    delete[] arr;
    return rot;

}

geom::mat3 *geom::RotZ(floatt angle)
{
    floatt *arr = new floatt[9]
                      {cos(angle), sin(angle), 0,
                       -sin(angle), cos(angle), 0,
                       0, 0, 1};
    mat3 *rot = new mat3(arr);
    delete[] arr;
    return rot;

}
