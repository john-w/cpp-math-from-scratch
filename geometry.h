#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "general.h"
#include "macros.h"

namespace geom
{
    struct Point3D
    {
        public:
        floatt x;
        floatt y;
        floatt z;

        Point3D(floatt x, floatt y, floatt z)
            : x(x), y(y), z(z) {}
        Point3D(Point3D &p)
            : x(p.x), y(p.y), z(p.z) {}

        void operator=(Point3D &p);
        void operator+(Point3D &p);
        void operator-(Point3D &p);
        void operator*(floatt alpha);
    };

    static Point3D Zero(0,0,0);

    struct Vect3D : public Point3D
    {
        public:
        floatt *coords[3]{&x, &y, &z};
        Vect3D(floatt x, floatt y, floatt z);
        Vect3D(Point3D &p);

        floatt norm(){return _norm;}
        void recalculateNorm();

        void operator=(Vect3D &v);
        void operator+(Vect3D &v);
        void operator-(Vect3D &v);
        void operator*(floatt alpha);
        bool operator>(Vect3D &v);
        bool operator<(Vect3D &v);

        private:
        floatt _norm;
    };

    class mat3
    {
        private:
        int _rank = 3;
        floatt *_arr = new floatt[static_cast<unsigned long>(_rank*_rank)];
        floatt _det;
        floatt _trace;

        void recalculateDet();
        void recalculateTrace();

        public:
        mat3();
        mat3(floatt *arr);
        mat3(floatt **arr);

        int rank() {return _rank;}
        floatt det() {return _det;}
        floatt trace() {return _trace;}

        void set(int i, int j, floatt val);
        mat3 T();

        floatt operator()(int i, int j);
        void operator=(mat3 &mat);
        void operator*(floatt scale);
        void operator+(mat3 &mat);
        void operator+(floatt val);
        void operator-(mat3 &mat);
        void operator-(floatt val);
        void operator*(mat3 &mat);
        Vect3D* operator*(Vect3D &v);

    };

    mat3 *RotX(floatt angle);
    mat3 *RotY(floatt angle);
    mat3 *RotZ(floatt angle);

}

#endif // GEOMETRY_H
