#include "datatypes.h"

template<class T>
LinkedList<T>::LinkedList()
{
    size = 0;
    head = nullptr;
    tail = nullptr;
}

template<class T>
LinkedList<T>::LinkedList(T val)
{
    size = 1;
    head = new Node<T>(val);
    tail = nullptr;
}

template<class T>
LinkedList<T>::~LinkedList()
{
    head->~Node();
}

template<class T>
T LinkedList<T>::append(T val)
{
    Node<T> *newNode = new LinkedList<T>(val);

    if (head == nullptr)
    {
        head = tail = newNode;
        size++;
    }
    else
    {
        tail->next = newNode;
        tail = tail->next;
        size++;
    }
}

template<class T>
T LinkedList<T>::prepend(T val)
{
    Node<T> *newNode = new LinkedList<T>(val);

    if (head == nullptr)
    {
        head = tail = newNode;
        size++;
    }
    else
    {
        newNode->next = head;
        head = newNode;
        size++;
    }
}

template<class T>
void LinkedList<T>::print()
{
#ifdef _GLIBCXX_IOSTREAM
    int i = 0;
    Node<T>* cur = head;
    std::cout << i << ": " << cur->value;
    cur = cur->next;

    while (cur != nullptr)
    {
        std::cout << " -> " << i << ": " << cur->value;
        i++;
    }
    std::cout << std::endl;
#else
#pragma message("STR(__func__) expects \'iostream\' to be included. Ignoring")
#endif
}

template<class T>
T LinkedList<T>::operator[](unsigned int index)
{
    Node<T> *cur =  (index > size-1) ? throw "Out of bounds" : head;
    while (index > 0)
    {
        cur = cur->next;
        index--;
    }
    return cur->value;
}

template<class T>
T LinkedList<T>::operator()(int index)
{
    index %= size;
    Node<T> *cur = head;
    while (index > 0)
    {
        cur = cur->next;
        index--;
    }
    return cur->value;
}
